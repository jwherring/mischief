custom_charge_schema =
  type: "object"
  properties:
    code:
      type: "string"
    chargeCode:
      type: "string"
      maxLength: 36
    quantity:
      type: "integer"
    eachAmount:
      type: "number"
    description:
      type: "string"
      maxLength: 255
    invoicePeriod:
      type: "string"
      pattern: "^(current|outstanding)$"
    remoteAddress:
      type: "string"
      maxLength: 128
  required: ['chargeCode', 'quantity', 'eachAmount']
  additionalProperties: false

destroy_charge_schema =
  type: "object"
  properties:
    chargeId:
      type: "string"
      maxLength: 36
    invoicePeriod:
      type: "string"
      pattern: "^(current|outstanding)$"
    remoteAddress:
      type: "string"
      maxLength: 128
  required: ['chargeId']
  additionalProperties: false

one_time_invoice_schema =
  type: "object"
  properties:
    code:
      type: "string"
  patternProperties:
    "charges\\[.*\\]\\[chargeCode\\]":
      type: "string"
      maxLength: 255
    "charges\\[.*\\]\\[quantity\\]":
      type: "integer"
      minimum: 1
    "charges\\[.*\\]\\[eachAmount\\]":
      type: "number"
    "charges\\[.*\\]\\[description\\]":
      type: "string"
      maxLength: 255
  additionalProperties: false
  required: ['code']

refund_schema =
  type: "object"
  properties:
    id:
      type: "string"
    amount:
      type: "number"
    remoteAddress:
      type: "string"
      maxLength: 128
  additionalProperties: false
  required: ['id', 'amount']

void_schema =
  type: "object"
  properties:
    id:
      type: "string"
    remoteAddress:
      type: "string"
      maxLength: 128
  additionalProperties: false
  required: ['id']

exports.custom_charge_schema = custom_charge_schema
exports.destroy_charge_schema = destroy_charge_schema
exports.one_time_invoice_schema = one_time_invoice_schema
exports.refund_schema = refund_schema
exports.void_schema = void_schema
