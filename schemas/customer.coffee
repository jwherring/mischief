customer_schema =
  type: "object"
  properties:
    id:
      type: "string"
    code:
      type: "string"
    firstName:
      type: "string"
      maxLength: 40
    lastName:
      type: "string"
      maxLength: 40
    email:
      type: "string"
      format: "email"
    company:
      type: "string"
      maxLength: 60
    isVatExempt:
      type: "integer"
      minimum: 0
      maximum: 1
    vatNumber:
      type: "string"
      maxLength: 32
    notes:
      type: "string"
      maxLength: 255
    firstContactDatetime:
      type: "string"
      format: "date-time"
    referer:
      type: "string"
      maxLength: 255
    campaignTerm:
      type: "string"
      maxLength: 255
    campaignName:
      type: "string"
      maxLength: 255
    campaignSource:
      type: "string"
      maxLength: 255
    campaignMedium:
      type: "string"
      maxLength: 255
    campaignContent:
      type: "string"
      maxLength: 255
    "subscription[planCode]":
      type: "string"
      maxLength: 255
    "subscription[couponCode]":
      type: "string"
      maxLength: 255
    "subscription[initialBillDate]":
      type: "string"
      format: "date-time"
    "subscription[method]":
      type: "string"
      maxLength: 255
    "subscription[ccNumber]":
      type: "string"
      maxLength: 255
    "subscription[ccType]":
      type: "string"
      maxLength: 255
    "subscription[ccLastFour]":
      type: "string"
      maxLength: 4
    "subscription[ccExpiration]":
      type: "string"
      maxLength: 255
    "subscription[ccExpirationDate]":
      type: "string"
      maxLength: 255
    "subscription[ccEmail]":
      type: "string"
      maxLength: 255
    "subscription[cancelType]":
      type: "string"
      maxLength: 255
    "subscription[cancelReason]":
      type: "string"
      maxLength: 255
    "subscription[canceledDatetime]":
      type: "string"
      maxLength: 255
    "subscription[createdDatetime]":
      type: "string"
      maxLength: 255
    "subscription[ccCardCode]":
      type: "string"
      maxLength: 255
    "subscription[ccFirstName]":
      type: "string"
      maxLength: 255
    "subscription[ccLastName]":
      type: "string"
      maxLength: 255
    "subscription[ccCompany]":
      type: "string"
      maxLength: 255
    "subscription[ccCountry]":
      type: "string"
      maxLength: 255
    "subscription[ccAddress]":
      type: "string"
      maxLength: 255
    "subscription[ccCity]":
      type: "string"
      maxLength: 255
    "subscription[ccState]":
      type: "string"
      maxLength: 255
    "subscription[ccZip]":
      type: "string"
      maxLength: 255
    "subscription[returnUrl]":
      type: "string"
      maxLength: 255
    "subscription[cancelUrl]":
      type: "string"
      maxLength: 255
    invoices:
      type: "array"
      items: "object"
    refererHost:
      type: "string"
      maxLength: 255
    key:
      type: "string"
      maxLength: 255
    remoteAddress:
      type: "string"
      maxLength: 255
    createdDatetime:
      type: "string"
      format: "date-time"
    modifiedDatetime:
      type: "string"
      format: "date-time"
  patternProperties:
    "charges\\[.*\\]\\[chargeCode\\]":
      type: "string"
      maxLength: 255
    "charges\\[.*\\]\\[quantity\\]":
      type: "integer"
      minimum: 1
    "charges\\[.*\\]\\[eachAmount\\]":
      type: "number"
    "charges\\[.*\\]\\[description\\]":
      type: "string"
      maxLength: 255
    "items\\[.*\\]\\[itemCode\\]":
      type: "string"
      maxLength: 36
    "items\\[.*\\]\\[quantity\\]":
      type: "number"
    "metaData\\[.*\\]":
      type: "string"
      maxLength: 255
  required: ["code", "firstName", "lastName", "email"]
  additionalProperties: false

search_schema =
  type: "object"
  properties:
    subscriptionStatus:
      type: "string"
      pattern: "^(active|cancelled)Only$"
    createdAfterDate:
      type: "string"
      format: "date"
    createdBeforeDate:
      type: "string"
      format: "date"
    canceledAfterDate:
      type: "string"
      format: "date"
    canceledBeforeDate:
      type: "string"
      format: "date"
    transactedAfterDate:
      type: "string"
      format: "date"
    transactedBeforeDate:
      type: "string"
      format: "date"
    orderBy:
      type: "string"
      pattern: "^(name|company|plan|billingDatetime|createdDatetime)$"
    orderByDirection:
      type: "string"
      pattern: "^(asc|desc)$"
    search:
      type: "string"
      maxLength: 128
  patternProperties:
    "^planCode\\[.*\\]$":
      type: "string"
      maxLength: 128
  additionalProperties: false

exports.customer_schema = customer_schema
exports.search_schema = search_schema
