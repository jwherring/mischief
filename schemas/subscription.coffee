subscription_schema =
  type: "object"
  properties:
    method:
      type: "string"
      maxLength: 255
    planCode:
      type: "string"
      maxLength: 255
    couponCode:
      type: "string"
      maxLength: 255
    initialBillDate:
      type: "string"
      format: "date-time"
    ccNumber:
      type: "string"
      maxLength: 255
    ccExpiration: #MM/YYYY
      type: "string"
      maxLength: 255
    ccEmail:
      type: "string"
      maxLength: 255
    cancelType:
      type: "string"
      maxLength: 255
    cancelReason:
      type: "string"
      maxLength: 255
    canceledDatetime:
      type: "string"
      maxLength: 255
    createdDatetime:
      type: "string"
      maxLength: 255
    ccCardCode:
      type: "string"
      maxLength: 255
    ccFirstName:
      type: "string"
      maxLength: 255
    ccLastName:
      type: "string"
      maxLength: 255
    ccCompany:
      type: "string"
      maxLength: 255
    ccCountry:
      type: "string"
      maxLength: 255
    ccAddress:
      type: "string"
      maxLength: 255
    ccCity:
      type: "string"
      maxLength: 255
    ccState:
      type: "string"
      maxLength: 255
    ccZip:
      type: "string"
      maxLength: 255
    returnUrl:
      type: "string"
      maxLength: 255
    changeBillDate:
      type: "string"
      maxLength: 255
    cancelUrl:
      type: "string"
      maxLength: 255
  additionalProperties: false
  required: ['planCode']
  #required: ['planCode', 'ccNumber', 'ccExpiration', 'ccCardCode', 'ccFirstName', 'ccLastName', 'ccCity', 'ccAddress', 'ccState', 'ccZip']

exports.subscription_schema = subscription_schema
