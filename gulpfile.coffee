gulp = require 'gulp'
coffee = require 'gulp-coffee'
mocha = require 'gulp-mocha'

gulp.task 'build', () ->
  gulp.src 'src/**/*.coffee'
    .pipe coffee()
    .pipe gulp.dest 'lib'

gulp.task 'test:cheddarclient', () ->
  gulp.src 'test/cheddarclient.coffee'
    .pipe mocha()

gulp.task 'test:cheddarcustomer', () ->
  gulp.src 'test/cheddarcustomer.coffee'
    .pipe mocha()

gulp.task 'test:trackeditems', () ->
  gulp.src 'test/trackeditems.coffee'
    .pipe mocha()

gulp.task 'test', ['test:cheddarclient', 'test:cheddarcustomer']
