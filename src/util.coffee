bb = require 'bluebird'
tv4 = require 'tv4'
formats = require 'tv4-formats'
validator = tv4.freshApi()
validator = bb.promisifyAll validator
validator.addFormat formats

validationPromise = (entity, schema) ->
  new bb (resolve, reject) ->
    if validator.validate entity, schema
      resolve {"valid": true}
    else
      reject validator.error

exports.validationPromise = validationPromise
