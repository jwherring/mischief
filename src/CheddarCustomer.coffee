uuid = require 'node-uuid'
bb = require 'bluebird'
vp = require('./util').validationPromise
tv4 = require 'tv4'
ld = require 'lodash'

cs = require '../schemas/customer'
customer_schema = cs.customer_schema
search_schema = cs.search_schema

cc = require '../schemas/charge'
custom_charge_schema = cc.custom_charge_schema
destroy_charge_schema = cc.destroy_charge_schema
one_time_invoice_schema = cc.one_time_invoice_schema
refund_schema = cc.refund_schema
void_schema = cc.void_schema

ss = require '../schemas/subscription'
subscription_schema = ss.subscription_schema

convert_subscription = (subscription) ->
  newsubscription = {}
  for k, v of subscription
    newsubscription["subscription[#{k}]"] = v
  newsubscription

convert_charges = (charges) ->
  newcharges = {}
  for k, v of charges
    charges["charges[#{k}]"] = v
  charges

extend_object = (target, source) ->
  for k, v of source
    target[k] = v
  target

incorporate_subscription = (newinformation) ->
  newsubscription = convert_subscription newinformation.subscription
  delete newinformation.subscription
  extend_object newinformation, newsubscription

process_customer = (p) ->
  if ld.isArray p.subscriptions.subscription
    p.subscription = p.subscriptions.subscription[0]
  else
    p.subscription = p.subscriptions.subscription
  delete p.subscriptions
  p.subscription.planCode = p.subscription.plans.plan.$.code
  delete p.subscription.$
  delete p.subscription.gatewayToken
  delete p.subscription.gatewayAccount
  delete p.subscription.plans
  ld.each p.subscription.items.item, (i) ->
    itemcode = i.$.code
    codestring = "items[#{itemcode}][itemCode]"
    quantstring = "items[#{itemcode}][quantity]"
    p[codestring] = itemcode
    p[quantstring] = parseInt i.quantity
  delete p.subscription.items
  if not ld.isArray p.subscription.invoices.invoice
    p.invoices = [p.subscription.invoices.invoice]
  else
    p.invoices = p.subscription.invoices.invoice
  delete p.subscription.invoices
  ld.each p.invoices, (i) ->
    if not ld.isArray i.charges.charge
      i.charges = [i.charges.charge]
    else
      i.charges = i.charges.charge
    ld.each i.charges, (ic) ->
      ic.id = ic.$.id
      ic.code = ic.$.code
      delete ic.$
    i.id = i.$.id
    delete i.$
  p.code ?= p.$.code
  p.id ?= p.$.id
  if p.isVatExempt?
    p.isVatExempt = parseInt p.isVatExempt
  delete p.$
  delete p.gatewayToken
  delete p.refererHost
  delete p.metaData
  p

store_customer_response = (that, res) ->
  newcustomer = res.customers.customer
  newcustomer.code = newcustomer.$.code
  that.code = newcustomer.$.code
  newcustomer.id = newcustomer.$.id
  that.id = newcustomer.$.id
  delete newcustomer.$
  newcustomer = process_customer newcustomer
  newcustomer = incorporate_subscription newcustomer
  that.body = newcustomer
  that.client.customers[newcustomer.code] = that
  that

CheddarCustomer = class CheddarCustomer

  constructor: (@client, @code) ->
    if not @client?
      throw new Error "CheddarCustomers must have access to a Cheddar API client"
    @getpath = 'customers/get'
    @postpath = 'customers/new'
    @putpath = 'customers/edit'
    @deletepath = 'customers/delete'
    @cancelpath = 'customers/cancel'
    @incrementpath = 'customers/add-item-quantity'
    @decrementpath = 'customers/remove-item-quantity'
    @setpath = 'customers/set-item-quantity'
    @addchargepath = 'customers/add-charge'
    @deletechargepath = 'customers/delete-charge'
    @onetimeinvoicepath = 'invoices/new'
    @refundinvoicepath = 'invoices/refund'
    @voidinvoicepath = 'invoices/void'
    @runoutstandingpath = 'customers/run-outstanding'
    @editsubscriptionpath = 'customers/edit-subscription'


  setBody: (body) ->
    body = process_customer body
    body = incorporate_subscription body
    if tv4.validate body, customer_schema
      @body = body
    else
      err = tv4.error
      console.log tv4.error
      throw new Error err
    @

  retrieve: (code) ->
    that = @
    code ?= @code
    if not code?
      throw new Error "You must supply an identifier to retrieve a Customer via the Cheddar API"
    params =
      code: code
    @client.makeGetRequest @getpath, params, {}
      .then (res) ->
        store_customer_response that, res
      .catch (err) ->
        that.body = {}
        that.error = err.message
        console.log "CUSTOMER #{code} not loaded (#{err.message})"
        that

  create: (newinformation) ->
    that = @
    newinformation = incorporate_subscription newinformation
    vp newinformation, customer_schema
      .then () ->
        that.client.makePostRequest that.postpath, newinformation
      .then (res) ->
        store_customer_response that, res
      .catch (err) ->
        that.body = {}
        that.error = err.message
        delete that.code
        delete that.id
        console.log "CUSTOMER not created (#{err.message})"
        that
  
  update: (newinformation) ->
    that = @
    if not @code?
      throw new Error "You must supply an identifier to retrieve a Customer via the Cheddar API"
    newinformation = incorporate_subscription newinformation
    put_customer_schema = JSON.parse JSON.stringify customer_schema
    delete put_customer_schema.required
    vp newinformation, put_customer_schema
      .then () ->
        that.client.makePutRequest that.putpath, that.code, newinformation
      .then (res) ->
        store_customer_response that, res
      .catch (err) ->
        console.log "CUSTOMER not updated (#{err.message})"
        that.error = err.message
        that

  destroy: () ->
    that = @
    if not @code?
      throw new Error "You must supply a code to delete a customer"
    params =
      code: @code
    @client.makeDeleteRequest @deletepath, params
      .then (res) ->
        delete that.client.customers[that.id]
        delete that.body
        delete that.id
        delete that.code
        that
      .catch (err) ->
        that.error = err.message
        console.log "CUSTOMER not deleted (#{err.message})"
        that

  cancel: () ->
    that = @
    params =
      code: @code
    @client.makeGetRequest @cancelpath, params, {}
      .then (res) ->
        store_customer_response that, res
        that
      .catch (err) ->
        that.error = err.message
        console.log "CUSTOMER not canceled (#{err.message})"
        that

  getItemRefString: (itemcode) ->
    "items[#{itemcode}][quantity]"

  getItemQuantity: (itemcode) ->
    refstring = @getItemRefString itemcode
    quantity = @body[refstring]
    quantity

  getChargeObject: (chargecode) ->
    chargeregexp = new RegExp "charges\\[.*\\]\\[.*\\]"
    chargekeys = (k for k of @body when chargeregexp.test k)
    console.log chargekeys
    chargekeys


  incrementItemQuantity: (itemcode, quantity) ->
    that = @
    quantity ?= 1
    params =
      code: @code
      quantity: quantity
      itemCode: itemcode
    @client.makePostRequest @incrementpath, params, {}
      .then (res) ->
        store_customer_response that, res
        that
      .catch (err) ->
        that.error = err.message
        console.log "ITEM not incremented (#{err.message})"
        that

  decrementItemQuantity: (itemcode, quantity) ->
    that = @
    quantity ?= 1
    params =
      code: @code
      quantity: quantity
      itemCode: itemcode
    @client.makePostRequest @decrementpath, params, {}
      .then (res) ->
        store_customer_response that, res
        that
      .catch (err) ->
        that.error = err.message
        console.log "ITEM not decremented (#{err.message})"
        that

  setItemQuantity: (itemcode, quantity) ->
    that = @
    quantity ?= 1
    params =
      code: @code
      quantity: quantity
      itemCode: itemcode
    @client.makePostRequest @setpath, params, {}
      .then (res) ->
        store_customer_response that, res
        that
      .catch (err) ->
        that.error = err.message
        console.log "ITEM not set (#{err.message})"
        that

  addCustomCharge: (params) ->
    that = @
    params ?= {}
    params.code = @code
    vp params, custom_charge_schema
      .then that.client.makePostRequest that.addchargepath, params, {}
      .catch (err) ->
        that.error = err.message
        console.log "CHARGE not added (#{err.message})"
        that

  destroyCustomCharge: (params) ->
    that = @
    params ?= {}
    params.code = @code
    vp params, destroy_charge_schema
      .then that.client.makePostRequest that.deletechargepath, params, {}
      .catch (err) ->
        that.error = err.message
        console.log "CHARGE not removed (#{err.message})"
        that

  getInvoices: () ->
    @body.invoices

  createOneTimeInvoice: (params) ->
    that = @
    newparams = {}
    newparams.code = params.code
    delete params.code
    chargecode = params.chargeCode
    noncode = (k for k of params when k isnt 'code')
    for k in noncode
      key = "charges[#{chargecode}][#{k}]"
      newparams[key] = params[k]
    console.log newparams
    vp newparams, one_time_invoice_schema
      .then that.client.makePostRequest that.onetimeinvoicepath, newparams, {}
      .catch (err) ->
        that.error = err.message
        console.log JSON.stringify err
        console.log "CHARGE not created (#{err.message})"
        that

  runInvoice: (params) ->
    that = @
    params ?= {}
    params.id = invoiceid
    vp params, void_schema
      .then that.client.makePostRequest that.runoutstandingpath, params, {}
      .catch (err) ->
        that.error = err.message
        console.log "CHARGE not voided (#{err.message})"
        that

  refundInvoice: (invoiceid, amount, params) ->
    that = @
    params ?= {}
    params.id = invoiceid
    params.amount = amount
    vp params, refund_schema
      .then that.client.makePostRequest that.refundinvoicepath, params, {}
      .catch (err) ->
        that.error = err.message
        console.log "CHARGE not refunded (#{err.message})"
        that

  voidInvoice: (invoiceid, params) ->
    that = @
    params ?= {}
    params.id = invoiceid
    vp params, void_schema
      .then that.client.makePostRequest that.voidinvoicepath, params, {}
      .catch (err) ->
        that.error = err.message
        console.log "CHARGE not voided (#{err.message})"
        that

  updateSubscription: (newplancode, params) ->
    that = @
    if not params.changeBillDate?
      params.changeBillDate = 'now'
    params.planCode = newplancode
    vp params, subscription_schema
      .then that.client.makePutRequest that.editsubscriptionpath, that.code, params, {}
      .catch (err) ->
        that.error = err.message
        console.log "SUBSCRIPTION not updated (#{err.message})"
        that

module.exports = CheddarCustomer
