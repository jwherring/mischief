bb = require 'bluebird'
ld = require 'lodash'
request = bb.promisifyAll require 'request'
xml = bb.promisifyAll require 'xml2js'
tv4 = require 'tv4'
customer_schema = require('../schemas/customer').customer_schema

vp = require('./util').validationPromise

search_schema = require('../schemas/customer').search_schema

CheddarCustomer = require './CheddarCustomer'

CheddarClient = class CheddarClient
  
  constructor: (@username, @password, @productcode=null) ->
    if arguments.length < 2
      throw new Error "You must supply at least a username and password to access the Cheddar API"
    @baseurl = "https://cheddargetter.com/xml/"
    @plans = {}
    @customers = {}
    @error = null
    @auth =
      username: @username
      password: @password
    if productcode?
      @setProductCode productcode

  setProductCode: (productcode) ->
    @plans = {}
    @customers = {}
    @productcode = productcode

  mergeObjects: (mine, other) ->
    for key, value of other
      mine[key] = other[key]
    mine

  makeParamsString : (params) ->
    "/" + ("#{k}/#{params[k]}" for k of params).join '/'
    
  makeUrl: (path, params=null) ->
    if not @productcode?
      throw new Error "You must set a product code before you can retrieve data from the Cheddar API"
    if params?
      params = @makeParamsString params
    else
      params = ""
    "#{@baseurl}#{path}/productCode/#{@productcode}#{params}"

  makeGetRequest: (path, params, config) ->
    that = @
    url = @makeUrl path
    if 'code' of params
      url = "#{url}/code/#{params.code}"
    delete params.code
    console.log "UPDATED URL"
    console.log url
    myconfig =
      'auth': @auth
      'url': url
    config = if config? then @mergeObjects myconfig, config else myconfig
    if params?
      config.qs = params
    request.getAsync config
      .then @parseResponse that
      .then @storeResponse that
      .catch (err) ->
        that.error = err.message
        bb.reject err.message

  makePostRequest: (path, params, config) ->
    that = @
    url = @makeUrl path
    if 'code' of params and path isnt 'customers/new'
      url = "#{url}/code/#{params.code}"
      delete params.code
    if 'itemCode' of params
      url = "#{url}/itemCode/#{params.itemCode}"
    myconfig =
      'auth': @auth
      'url': url
      'form': params
    config = if config? then @mergeObjects myconfig, config else myconfig
    request.postAsync config
      .then @parseResponse that
      .then @storeResponse that
      .catch (err) ->
        that.error = err.message
        bb.reject err.message

  makePutRequest: (path, code, params, config) ->
    that = @
    url = @makeUrl path
    url = "#{url}/code/#{code}"
    myconfig =
      'auth': @auth
      'url': url
      'form': params
    config = if config? then @mergeObjects myconfig, config else myconfig
    request.postAsync config
      .then @parseResponse that
      .then @storeResponse that
      .catch (err) ->
        that.error = err.message
        bb.reject err.message


  makeDeleteRequest: (path, params, config) ->
    that = @
    url = @makeUrl path, params
    myconfig =
      'auth': @auth
      'url': url
    config = if config? then @mergeObjects myconfig, config else myconfig
    request.delAsync config
      .catch (err) ->
        that.error = err.message
        bb.reject err.message
    

  parseResponse: (context) ->
    (res) ->
      res = res[0]
      context.statusCode = res.statusCode
      if context.statusCode is 200
        xml.parseStringAsync res.body, {explicitArray: false}
      else
        throw new Error "Invalid response: #{res.statusCode}"

  storeResponse: (context) ->
    (res) ->
      context.body = res
      res

  getAllPricingPlans: (params) ->
    that = @
    path = 'plans/get'
    params ?= {}
    @makeGetRequest path, params, {}
      .then (res) ->
        if res.plans.plan.constructor isnt Array
          res.plans.plan = [res.plans.plan]
        res.plans.plan.forEach (p) ->
          id = p.$.id
          code = p.$.code
          p.id = id
          delete p.$
          that.plans[code] = p
        that
      .catch (err) ->
        that.error = err.message
        bb.reject err.message

  getSinglePricingPlan: (plancode) ->
    that = @
    params =
      code: plancode
    path = 'plans/get'
    @makeGetRequest path, params, {}
      .then (res) ->
        delete res.plans.plan.$
        that.plans[plancode] = res.plans.plan
        that
      .catch (err) ->
        that.error = err.message
        bb.reject err.message

  getAllPromotions: () ->
    that = @
    path = 'promotions/get'
    @makeGetRequest path, {}, {}
      .then (res) ->
        that
      .catch (err) ->
        that.error = err.message
        bb.reject err.message

  getSinglePromotion: (promotioncode) ->
    that = @
    path = 'promotions/get'
    params =
      code: promotioncode
    @makeGetRequest path, {}, {}
      .then (res) ->
        that
      .catch (err) ->
        that.error = err.message
        bb.reject err.message

  getAllCustomers: (params) ->
    that = @
    that.customers = {}
    path = 'customers/get'
    params ?= {}
    vp params, search_schema
      .then () ->
        that.makeGetRequest path, params, {}
      .then (res) ->
        if res.customers.customer.constructor isnt Array
          res.customers.customer = [res.customers.customer]
        res.customers.customer.forEach (p) ->
          code = p.$.code
          try
            if code?
              that.customers[code] = new CheddarCustomer that, code
              that.customers[code].setBody p
          catch e
            console.log "ERROR"
            console.log e
            console.log p
        that
      .catch (err) ->
        that.error = err.message
        bb.reject err.message

  refreshCustomers: () ->
    that = @
    bb.all (@customers[k].retrieve() for k of @customers)
      .then () ->
        that
      .catch (err) ->
        that.error = err.message
        bb.reject err.message


module.exports = CheddarClient
