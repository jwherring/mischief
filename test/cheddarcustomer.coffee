chai = require 'chai'
should = chai.should()
chaiaspromised = require 'chai-as-promised'
chai.use chaiaspromised
uuid = require 'node-uuid'
config = require './config'
CheddarClient = require '../src/CheddarClient'
CheddarCustomer = require '../src/CheddarCustomer'

describe 'CheddarCustomer creation test', () ->

  client = {}

  before () ->
    client = new CheddarClient(config.username, config.password, config.productcode)

  it 'should not be possible to create a CheddarCustomer without a client', () ->
    try
      cc = new CheddarCustomer()
    catch e
      e.message.should.equal 'CheddarCustomers must have access to a Cheddar API client'

  it 'should not be possible to retrieve a customer without a supplied code', () ->
    try
      cc = new CheddarCustomer(client)
    catch e
      e.message.should.equal 'You must supply an identifier to retrieve a Customer via the Cheddar API'

  it 'should be possible to retrieve a customer with a valid code', () ->
    cc = new CheddarCustomer(client)
    customer = cc.retrieve(config.customercode)
      .then (c) ->
        c.should.have.deep.property 'client.statusCode', 200
        c.should.have.deep.property 'body.firstName'
        c.should.have.property 'id'
        c.should.have.property 'code'
        c.should.have.deep.property "client.customers.#{c.code}"
        c

  it 'should not be possible to retrieve a customer with an invalid code', () ->
    cc = new CheddarCustomer(client)
    customcode = "#{config.customercode}junk"
    customer = cc.retrieve customcode
      .then (c) ->
        client.statusCode.should.equal 404
      .catch (err) ->
        err.message.should.equal "Invalid response: 404"

  it 'should not be possible to create a customer without a code', () ->
    params =
      firstName: config.firstname
      lastName: config.lastname
      email: config.email
      "subscription[planCode]": config.subscriptionplancode
    cc = new CheddarCustomer(client)
    customer = cc.create(params)
      .catch (err) ->
        err.message.should.equal 'Missing required property: code'
          .should.not.have.property.code

  it 'should be possible to create and update and then delete a new customer', () ->
    newcode = uuid.v4()
    params =
      code: newcode
      firstName: config.firstname
      lastName: config.lastname
      email: config.email
      subscription:
        planCode: config.subscriptionplancode
    update_params =
      firstName: "NewFirst"
    cc = new CheddarCustomer(client)
    customer = cc.create(params)
    customer.then (res) ->
        cc.should.have.property 'id'
        cc.should.have.property 'code', newcode
        cc.should.have.deep.property 'body.firstName'
        cc.update update_params
      .then (res) ->
        cc.should.have.property 'id'
        cc.should.have.property 'code', newcode
        cc.should.have.deep.property 'body.firstName', update_params.firstName
        cc.destroy()
      .then (res) ->
        cc.should.not.have.property 'code'
        cc.should.not.have.property 'id'
        cc.should.not.have.property 'body'
      .catch (err) ->
        console.log "ERROR CREATING CUSTOMER"
        console.log err.message
        {}.should.have.property 'failed'
        err

  it 'should be possible to create a new customer and then cancel his subscription', () ->
    newcode = uuid.v4()
    params =
      code: newcode
      firstName: config.firstname
      lastName: config.lastname
      email: config.email
      subscription:
        planCode: config.subscriptionplancode
    cc = new CheddarCustomer(client)
    customer = cc.create(params)
    customer.then (res) ->
        cc.cancel()
      .then (res) ->
        res.body.should.satisfy (i) ->
          i['subscription[canceledDatetime]'].length > 0
        res
      .then (res) ->
        cc.destroy()
      .catch (err) ->
        console.log "ERROR CANCELING CUSTOMER"
        console.log err.message
        {}.should.have.property 'failed'
        err

