chai = require 'chai'
should = chai.should()
chaiaspromised = require 'chai-as-promised'
chai.use chaiaspromised
uuid = require 'node-uuid'
config = require './config'
CheddarClient = require '../src/CheddarClient'
CheddarCustomer = require '../src/CheddarCustomer'

describe 'Tracked item test', () ->

  client = {}

  before () ->
    client = new CheddarClient(config.username, config.password, config.productcode)

  it 'should be possible to create a customer and confirm that he has 0 tracked items on creation', () ->
    cc = new CheddarCustomer(client)
    newcode = uuid.v4()
    params =
      code: newcode
      firstName: config.firstname
      lastName: config.lastname
      email: config.email
      subscription:
        planCode: config.subscriptionplancode
    cc.create params
      .then (res) ->
        quant = cc.getItemQuantity config.trackeditemcode
        console.log "QUANT: #{quant}"
        quant.should.equal 0
        cc.incrementItemQuantity config.trackeditemcode
      .then (res) ->
        quant = cc.getItemQuantity config.trackeditemcode
        console.log "QUANT: #{quant}"
        quant.should.equal 1
        cc.decrementItemQuantity config.trackeditemcode
      .then (res) ->
        quant = cc.getItemQuantity config.trackeditemcode
        console.log "QUANT: #{quant}"
        quant.should.equal 0
        cc.setItemQuantity config.trackeditemcode, 1
      .then (res) ->
        quant = cc.getItemQuantity config.trackeditemcode
        console.log "QUANT: #{quant}"
        quant.should.equal 1
        cc.destroy()
      .then (res) ->
        res
      .catch (err) ->
        console.log "INITIAL QUANTITY is wrong"
        console.log err
        {}.should.equal 'failed'

