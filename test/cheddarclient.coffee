chai = require 'chai'
should = chai.should()
config = require './config'
CheddarClient = require '../src/CheddarClient'

describe 'CheddarClient creation test', () ->

  it.skip 'should throw an error on any attempt to create a client instance without a username and password', () ->
    try
      cc = new CheddarClient()
    catch e
      console.log e.message
      e.message.should.equal 'You must supply at least a username and password to access the Cheddar API'


  it.skip 'should throw the same error if only one of these is supplied', () ->
    try
      cc = new CheddarClient('username@mail.com')
    catch e
      console.log e.message
      e.message.should.equal 'You must supply at least a username and password to access the Cheddar API'

  it.skip 'should set a baseurl if username and password are supplied', () ->
    cc = new CheddarClient(config.username, config.password)
    cc.should.have.property 'baseurl'
    cc.baseurl.should.equal config.baseurl

  it.skip 'should not be able to make a request url without a product code', () ->
    try
      cc = new CheddarClient(config.username, config.password)
      cc.makeUrl 'path', null
    catch e
      e.message.should.equal "You must set a product code before you can retrieve data from the Cheddar API"

  it.skip 'should make a request url when supplied with a product code', () ->
    cc = new CheddarClient(config.username, config.password)
    cc.setProductCode config.productcode
    url = cc.makeUrl 'path', null
    url.should.equal "https://cheddargetter.com/xml/path/productCode/#{config.productcode}"
    

describe 'CheddarClient request dictionary tests', () ->

  cc = {}

  before () ->
    cc = new CheddarClient(config.username, config.password, config.productcode)

  it.skip 'should be possible to get a list of pricing plans', () ->
    response = cc.getAllPricingPlans()
      .then (res) ->
        res.should.have.property 'plans'
      .catch (err) ->
        console.log "ERROR IN GET ALL PRICING PLANS"
        console.log err.message
        cc.body.should.equal err

  it.skip 'should be possible to get a single pricing plan by code', () ->
    response = cc.getSinglePricingPlan(config.plancode)
      .then (res) ->
        dp = "plans.#{config.plancode}"
        res.should.have.deep.property dp
      .catch (err) ->
        console.log "ERROR IN GET PRICING PLAN"
        console.log err.message
        cc.body.should.equal err

  it.skip 'should be possible to get a list of customers', () ->
    response = cc.getAllCustomers({canceledAfterDate: config.canceledafterdate})
      .then (res) ->
        res.should.have.property 'customers'
      .catch (err) ->
        console.log "ERROR IN GET ALL CUSTOMERS"
        console.log err.message
        cc.body.should.equal err

  it 'should be possible to get all promotions', () ->
    response = cc.getAllPromotions()
      .catch (err) ->
        console.log "ERROR IN GET ALL PROMOTIONS"
        console.log err.message
        err
